!pip install xlsxwriter
!pip install qrcode

import xlsxwriter
import qrcode
import random
import os
import time
start_time = time.time()




# Create a workbook and add a worksheet.
workbook = xlsxwriter.Workbook('Expenses01.xlsx')
worksheet = workbook.add_worksheet()


def create_qr(data,name):
    qr = qrcode.QRCode(
        version=1,
        box_size=2,
        border=5
    )

    qr.add_data(data)
    qr.make(fit=True)
    img = qr.make_image(fill='black', back_color='white')
    img.save(name)


def clear_all():
    mydir = os.getcwd()

    filelist = [f for f in os.listdir(mydir) if f.endswith(".png")]
    for f in filelist:
        os.remove(os.path.join(mydir, f))


worksheet.set_column(2,3,11)
worksheet.set_default_row(48.75)

# Start from the first cell. Rows and columns are zero indexed.
row = 0
col = 0
random.seed()

# Iterate over the data and write it out row by row.
for num in range(0,1000,1):

    cost =random.randint(25, 1500)
    filename = 'test_qr'
    filename = filename + str(num) + '.png'
    worksheet.write(row, col,     num)
    worksheet.write(row, col + 1, cost)
    create_qr(cost,filename)
    worksheet.insert_image(row,col+2,filename)
    row += 1



# Write a total using a formula.
worksheet.write(row, 0, 'Total')
worksheet.write(row, 1, '=SUM(B1:B4)')

workbook.close()
clear_all()
print("--- %s seconds ---" % (time.time() - start_time))
